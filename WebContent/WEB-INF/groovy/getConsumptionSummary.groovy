#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "SELECT monthly_avg, weekly_avg, daily_avg, this_month_total, this_week_total, today_total, first_reading.first_reading_date_time, last_reading.last_reading_date_time FROM " +

		"(SELECT smart_meter_oid, SUM(new_cons - old_cons)/SUM(days) daily_avg FROM " +
		" (SELECT smart_meter_oid, T1.new_cons, T2.old_cons, T1.new_time, T2.old_time, TIMESTAMPDIFF(DAY, T2.old_time, T1.new_time) days " +
		" FROM   " +
		"	(SELECT smart_meter_oid, @curRow1 ::= @curRow1 + 1 new_row_nr, T1.new_cons, T1.new_time   " +
		"		FROM     " +
		"		(SELECT m.smart_meter_oid, MAX(m.reading_date_time) new_time, MAX(total_consumption) new_cons   " +
		"		FROM meter_reading m   " +
		"			LEFT OUTER JOIN household h ON m.smart_meter_oid  = h.smart_meter_oid " +
		"			LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
		"			LEFT OUTER JOIN user u ON u.oid = nu.user_oid    " +
		"		   JOIN (SELECT @curRow1 ::= 0) r     " +
		"		WHERE u.oid = \'" + user_id + "\' " +
		"		GROUP BY DATE(m.reading_date_time) " +
		"		ORDER BY m.reading_date_time DESC) T1) T1 INNER JOIN   " +
		"	(SELECT @curRow2 ::= @curRow2 + 1 old_row_nr, T2.old_cons, T2.old_time   " +
		"		FROM     " +
		"		(SELECT MAX(m.reading_date_time) old_time, MAX(total_consumption) old_cons   " +
		"		FROM meter_reading m " +
		"			LEFT OUTER JOIN household h ON m.smart_meter_oid  = h.smart_meter_oid " +
		"			LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
		"			LEFT OUTER JOIN user u ON u.oid = nu.user_oid  " +
		"		   JOIN (SELECT @curRow2 ::= 0) r " +
		"		WHERE u.oid = \'" + user_id + "\' " +
		"		GROUP BY DATE(m.reading_date_time)  " +
		"		ORDER BY m.reading_date_time DESC) T2) T2 ON T2.old_row_nr - 1 = T1.new_row_nr)M " +
		" WHERE " +
		"		new_cons-old_cons>=0 and days>0) avrg LEFT OUTER JOIN " +

		"(SELECT smart_meter_oid, AVG((new_cons - old_cons)/days *30) monthly_avg FROM " +
		"	(SELECT smart_meter_oid, T1.new_cons, T2.old_cons, T1.new_time, T2.old_time, TIMESTAMPDIFF(DAY, T2.old_time,T1.new_time) days " +
		"	FROM   " +
		"		(SELECT smart_meter_oid, @curRow1 ::= @curRow1 + 1 new_row_nr, T1.new_cons, T1.new_time   " +
		"			FROM     " +
		"			(SELECT m.smart_meter_oid, MAX(m.reading_date_time) new_time, MAX(total_consumption) new_cons   " +
		"			FROM meter_reading m   " +
		"				LEFT OUTER JOIN household h ON m.smart_meter_oid  = h.smart_meter_oid " +
		"				LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
		"				LEFT OUTER JOIN user u ON u.oid = nu.user_oid    " +
		"			   JOIN (SELECT @curRow1 ::= 0) r     " +
		"			WHERE u.oid = \'" + user_id + "\' " +
		"			GROUP BY YEAR(m.reading_date_time), MONTH(m.reading_date_time) " +
		"			ORDER BY m.reading_date_time DESC) T1) T1 INNER JOIN   " +
		"		(SELECT @curRow2 ::= @curRow2 + 1 old_row_nr, T2.old_cons, T2.old_time   " +
		"			FROM     " +
		"			(SELECT MAX(m.reading_date_time) old_time, MAX(total_consumption) old_cons  " + 
		"			FROM meter_reading m " +
		"				LEFT OUTER JOIN household h ON m.smart_meter_oid  = h.smart_meter_oid " +
		"				LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
		"				LEFT OUTER JOIN user u ON u.oid = nu.user_oid  " +
		"			   JOIN (SELECT @curRow2 ::= 0) r " +
		"			WHERE u.oid = \'" + user_id + "\' " +
		"			GROUP BY YEAR(m.reading_date_time), MONTH(m.reading_date_time)   " +
		"			ORDER BY m.reading_date_time DESC) T2) T2 ON T2.old_row_nr - 1 = T1.new_row_nr)M " +
		"WHERE " +
		"	new_cons-old_cons>=0) mavrg ON mavrg.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN  " +
	
		"(SELECT smart_meter_oid, SUM(new_cons - old_cons)/SUM(TIMESTAMPDIFF(WEEK, old_time, new_time)) weekly_avg FROM " +
		"	(SELECT smart_meter_oid, T1.new_cons, T2.old_cons, T1.new_time, T2.old_time, TIMESTAMPDIFF(WEEK, T2.old_time,T1.new_time) weeks " +
		"	FROM   " +
		"		(SELECT smart_meter_oid, @curRow1 ::= @curRow1 + 1 new_row_nr, T1.new_cons, T1.new_time   " +
		"			FROM     " +
		"			(SELECT m.smart_meter_oid, MAX(m.reading_date_time) new_time, MAX(total_consumption) new_cons   " +
		"			FROM meter_reading m   " +
		"				LEFT OUTER JOIN household h ON m.smart_meter_oid  = h.smart_meter_oid " +
		"				LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
		"				LEFT OUTER JOIN user u ON u.oid = nu.user_oid    " +
		"			   JOIN (SELECT @curRow1 ::= 0) r     " +
		"			WHERE u.oid = \'" + user_id + "\' " +
		"			GROUP BY YEAR(m.reading_date_time), WEEK(m.reading_date_time, 7) " +
		"			ORDER BY m.reading_date_time DESC) T1) T1 INNER JOIN   " +
		"		(SELECT @curRow2 ::= @curRow2 + 1 old_row_nr, T2.old_cons, T2.old_time   " +
		"			FROM     " +
		"			(SELECT MAX(m.reading_date_time) old_time, MAX(total_consumption) old_cons  " + 
		"			FROM meter_reading m " +
		"				LEFT OUTER JOIN household h ON m.smart_meter_oid  = h.smart_meter_oid " +
		"				LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
		"				LEFT OUTER JOIN user u ON u.oid = nu.user_oid  " +
		"			   JOIN (SELECT @curRow2 ::= 0) r " +
		"			WHERE u.oid = \'" + user_id + "\' " +
		"			GROUP BY YEAR(m.reading_date_time), WEEK(m.reading_date_time, 7)   " +
		"			ORDER BY m.reading_date_time DESC) T2) T2 ON T2.old_row_nr - 1 = T1.new_row_nr)M " +
		"WHERE " +
		"	new_cons-old_cons>0 and weeks >0) wavrg ON wavrg.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN  " +
		
		"(SELECT mr.smart_meter_oid, MAX(mr.total_consumption_adjusted)-MIN(mr.total_consumption_adjusted) today_total " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid  " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE DATE(mr.reading_date_time) = (SELECT DATE(mr.reading_date_time) " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE user_oid = \'" + user_id + "\' "+
		" ORDER BY DATE(mr.reading_date_time) DESC LIMIT 1) and user_oid = \'" + user_id +"\' "+
		" GROUP BY mr.smart_meter_oid) today ON today.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN " +
		
		"(SELECT mr.smart_meter_oid, ifnull(MAX(mr.total_consumption_adjusted)-MIN(mr.total_consumption_adjusted), 0) this_month_total " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		"= h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE MONTH(mr.reading_date_time) = (SELECT MONTH(mr.reading_date_time) " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE user_oid = \'" + user_id +"\' " +
		" ORDER BY DATE(mr.reading_date_time) DESC LIMIT 1) and " +
		" YEAR(mr.reading_date_time) = YEAR(NOW()) and " +
		" user_oid = \'" + user_id +"\' " +
		" GROUP BY mr.smart_meter_oid) monthly ON monthly.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN " +
		
		"(SELECT mr.smart_meter_oid, MAX(mr.total_consumption_adjusted)-MIN(mr.total_consumption_adjusted) this_week_total " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE WEEK(mr.reading_date_time, 7) = (SELECT WEEK(mr.reading_date_time, 7) " +
		"FROM meter_reading mr LEFT OUTER JOIN household h ON mr.smart_meter_oid " +
		" = h.smart_meter_oid  LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE user_oid = \'" + user_id +"\' " +
		" ORDER BY DATE(mr.reading_date_time) DESC LIMIT 1) and " +
		" YEAR(mr.reading_date_time) = YEAR(NOW()) and " +
		" user_oid = \'" + user_id + "\' "+
		" GROUP BY mr.smart_meter_oid) weekly ON weekly.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN " +
		
		"(SELECT mr.smart_meter_oid, mr.reading_date_time first_reading_date_time " +
		"FROM meter_reading mr   " +
		"LEFT OUTER JOIN household h ON mr.smart_meter_oid = h.smart_meter_oid " +
		"LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
        "LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +
		"WHERE u.oid = \'" + user_id +"\' "+
		" ORDER BY mr.reading_date_time LIMIT 1) first_reading ON first_reading.smart_meter_oid = avrg.smart_meter_oid LEFT OUTER JOIN " + 
		
		"(SELECT mr.smart_meter_oid, mr.reading_date_time last_reading_date_time " +  
		"FROM meter_reading mr " +  
		"LEFT OUTER JOIN household h ON mr.smart_meter_oid = h.smart_meter_oid " +   
		"LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +   
        "LEFT OUTER JOIN user u ON u.oid = nu.user_oid " +   
		"WHERE u.oid = \'" + user_id +"\' "+
		" ORDER BY mr.reading_date_time DESC LIMIT 1) last_reading ON last_reading.smart_meter_oid = avrg.smart_meter_oid" 

//println query

def result=null
def jsonString = null

def average = 0;
def ris=0
try {


	result = session.createSQLQuery(query).list()

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeNumberField("monthly_avg", r[0]!=null?new Double(r[0]).doubleValue():0)
	    jGenerator.writeNumberField("weekly_avg", r[1]!=null?new Double(r[1]).doubleValue():0)
	 	jGenerator.writeNumberField("daily_avg", r[2]!=null?new Double(r[2]).doubleValue():0)
	 	jGenerator.writeNumberField("this_month_total", r[3]!=null?new Double(r[3]).doubleValue():0)
	 	jGenerator.writeNumberField("this_week_total", r[4]!=null?new Double(r[4]).doubleValue():0)
	 	jGenerator.writeNumberField("today_total", r[5]!=null?new Double(r[5]).doubleValue():0)
	 	jGenerator.writeStringField("from", r[6].toString())
	 	jGenerator.writeStringField("to", r[7].toString())
	    jGenerator.writeEndObject()

	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}catch (Exception e) {

    e.printStackTrace();

}

commit(session)

println json
ArrayNode mapper = new ObjectMapper().readTree(json);
println mapper
return ["json" : mapper]