#input user_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "SELECT T2.new_cons - T1.old_cons adj_qty, date(T1.old_time) adj_date " +
		"FROM (SELECT @curRow1 ::= @curRow1 + 1 old_row_nr, T1.old_cons, T1.old_time " +
		"FROM " +
		"(SELECT " +
		"	MAX(total_consumption_adjusted) old_cons, " +
		"	MAX(m.reading_date_time) old_time " +
		"FROM meter_reading m " +
		"	LEFT OUTER JOIN smart_meter sm ON sm.oid = m.smart_meter_oid " +   
		"	LEFT OUTER JOIN household h ON sm.oid = h.smart_meter_oid " +
		"	LEFT OUTER JOIN building b ON b.oid = h.building_oid " +
		"	LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"	LEFT OUTER JOIN user u ON u.oid = nu.user_oid  " +
		"	JOIN (SELECT @curRow1 ::= 0) r " +
		"WHERE u.oid = \'" +user_id +"\'  AND total_consumption_adjusted IS NOT NULL " +
		"	GROUP BY date(m.reading_date_time) " +
		"	ORDER BY old_time desc) T1) T1 INNER JOIN " +
		"(SELECT @curRow2 ::= @curRow2 + 1 new_row_nr, T2.new_cons, T2.new_time " +
		"FROM " +
		"(SELECT " +
		"	MAX(total_consumption_adjusted) new_cons, " +
		"	MAX(m.reading_date_time) new_time " +
		"FROM meter_reading m " +
		"	LEFT OUTER JOIN smart_meter sm ON sm.oid = m.smart_meter_oid " +   
		"	LEFT OUTER JOIN household h ON sm.oid = h.smart_meter_oid " +
		"	LEFT OUTER JOIN building b ON b.oid = h.building_oid " +
		"	LEFT OUTER JOIN neutral_user nu ON nu.household_oid = h.oid " +
		"	LEFT OUTER JOIN user u ON u.oid = nu.user_oid  " +
		"	JOIN (SELECT @curRow2 ::= 1) r " +
		"WHERE u.oid = \'" +user_id +"\'   AND total_consumption_adjusted IS NOT NULL " +   
		"	GROUP BY date(m.reading_date_time) " +
		"	ORDER BY new_time desc) T2) T2 ON T2.new_row_nr = T1.old_row_nr"

//println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()

	if(result[0]==null || result.size()==0)
	{
			jGenerator.writeStartObject()
		    jGenerator.writeNumberField("quantity", new Double("0"))
		 	jGenerator.writeStringField("timestamp", new String(""))
		    jGenerator.writeEndObject()
	}
	else
	    for (r in result)
	    {
	    	//write json
	    	jGenerator.writeStartObject()
		    jGenerator.writeNumberField("quantity", new Double(r[0]).doubleValue())
		 	jGenerator.writeStringField("timestamp", r[1].toString())
		    jGenerator.writeEndObject()
		}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

//println json
ArrayNode mapper = new ObjectMapper().readTree(json);
//println mapper
return ["json" : mapper]